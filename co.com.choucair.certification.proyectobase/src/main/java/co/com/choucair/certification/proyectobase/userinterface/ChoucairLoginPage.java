package co.com.choucair.certification.proyectobase.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class ChoucairLoginPage extends PageObject {
    public static final Target LOGIN_BUTTON = Target.the("Button that shows us the form to login").located(By.xpath("//div[@class='d-none d-lg-block']//strong[contains(text(),'Ingresar')]"));
    public static final Target INPUT_USER = Target.the("Input text from user").located(By.id("username"));
    public static final Target INPUT_PASSWORD = Target.the("Input password from password").located(By.name("password"));
    public static final Target ENTER_BUTTON = Target.the("Button submit for login").located(By.xpath("//button[contains(@class,'btn btn-primary')]"));
}


//*[@id="Ingresar"]/div/div/div[2]/div[2]/form/div[3]/button