package co.com.choucair.certification.proyectobase.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class SearchCoursePage extends PageObject {

    public static final Target BUTTON_UC = Target.the("Seleccion de universidad choucair").located(By.xpath("//div[@id='certificaciones']//strong"));
    public static final  Target INPUT_COURSE = Target.the("Buscar el curso").located(By.xpath("//input[@id='coursesearchbox']"));
    public static final Target BUTTON_GO = Target.the("Click en buscar").located(By.xpath("//button[@class='btn btn-secondary']"));
    public static final Target SELECT_COURSE = Target.the("Seleccion de curso").located(By.xpath("//h4[@class='result-title']//a[contains(text(),'Examen Ingresos - Nivel 1')]"));

}
